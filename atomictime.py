"""
AtomicTime.py
Gets the time from time.nist.gov and displays it
Author: Richard Harrington
Date Created: 9/24/13
"""

import ntplib
from time import ctime

c=ntplib.NTPClient()
response=c.request('time.nist.gov')

print(ctime(response.tx_time))
